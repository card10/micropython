micropython fork with card10 support
------------------------------------

Original README in [`README-OLD.md`](./README-OLD.md).
`card10` specific code is in [`ports/card10`](./ports/card10).

## Building

Build the card10 port using
```bash
cd ports/card10
make SDK_LOCATION=/path/to/firmware/sdk
```
If everything goes well, you'll end up with a file `ports/card10/build/firmware.elf`.
